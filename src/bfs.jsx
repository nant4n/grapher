import { areNodesEqual, edgeExists } from "./App";

export const bfs = (nodeList, linkList, source, target) => {
  const queue = [source];
  
  const path = {};
  const visited = new Set();
  console.dir(linkList);
  while (queue.length !== 0) {
    const currNode = dequeue(queue);
    visited.add(getNodeHash(currNode));
    if (areNodesEqual(currNode, target)) break;
    nodeList.filter(
      n => (edgeExists(linkList, currNode, n) || edgeExists(linkList, n, currNode)) && !visited.has(getNodeHash(n))
    ).forEach(nextNode => {
      path[nextNode.id] = currNode.id;
      queue.push(nextNode);
    });
  }
  
  const pathList = [target.id];
  let currNode = target.id;
  console.dir(JSON.parse(JSON.stringify(path)));
  console.dir(JSON.parse(JSON.stringify(currNode)));
  console.dir(JSON.parse(JSON.stringify(source)));
  let counter = 5;
  while (currNode !== source.id) {
    currNode = path[currNode];
    pathList.push(currNode);
    counter--;
    if (counter === 0) break;
  }

  return pathList.reverse();
};

const getNodeHash = (node) => {
  return node.x.toString() + '|' + node.y.toString();
}

const dequeue = (q) => q.splice(0, 1)[0];