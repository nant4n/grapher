
export const BACKGROUND_COLOR = "#272727";  // #131313, #212121, #272727
export const SECTION_COLOR = "#323232"; // 'brown' is oddly kinda nice
export const SECTION_BORDER = "4px solid #818181";
export const CONTENT_CARD_COLOR = "rgb(60, 60, 60)";
export const PRIMARY_TEXT_COLOR = "";
export const SECONDARY_TEXT_COLOR = "";

export const CATEGORY_LIST = ['diamond', 'club', 'heart', 'spade'];
export const CATEGORY_MAP = {
  'diamond': '♦',
  'club': '♣',
  'heart': '♥',
  'spade': '♠',
}

export const DIFFICULTY_MAP = {
  1: 'A',
  2: '2',
  3: '3',
  4: '4',
  5: '5',
  6: '6',
  7: '7',
  8: '8',
  9: '9',
  10: '10',
  11: 'J',
  12: 'Q',
  13: 'K',
}
export const TIME_PERIOD_MAP = {
  'daily': 'D',
  'weekly': 'W',
  'infinite': '∞'
}

export const DIFFICULTY_VALUES_LIST = [
  { value: 1, label: 'A', },
  { value: 2, label: '2', },
  { value: 3, label: '3', },
  { value: 4, label: '4', },
  { value: 5, label: '5', },
  { value: 6, label: '6', },
  { value: 7, label: '7', },
  { value: 8, label: '8', },
  { value: 9, label: '9', },
  { value: 10, label: '10', },
  { value: 11, label: 'J', },
  { value: 12, label: 'Q', },
  { value: 13, label: 'K', },
];