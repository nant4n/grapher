import React from 'react';
import { Graph } from '@visx/network';
import './App.css';
import Modal from '@mui/material/Modal';
import { Box } from '@mui/material';
import ParkIcon from '@mui/icons-material/Park';
import { Park } from '@mui/icons-material';
import { bfs } from './bfs';

const CustomNode = ({ node, onClick, isSelected }) => (
  <g onClick={onClick}>
    <circle cx={node.x} cy={node.y} r={15} fill={isSelected ? "pink" : "lightblue"} />
    <text x={node.x} y={node.y} textAnchor="middle" alignmentBaseline="middle" fill="black" color='black'>
      {node.id}
    </text>
  </g>
);

export const background = '#272b4d';
export const areNodesEqual = (n1, n2) => {
  return n1.id === n2.id;
}
export const areEdgesEqual = (e1, e2) => {
  return areNodesEqual(e1.source, e2.source) && areNodesEqual(e1.target, e2.target);
}
export const edgeExists = (linkList, source, target) => {
  return linkList.some(edge => areNodesEqual(edge.source, source) && areNodesEqual(edge.target, target));
}

const width = "95vw";
const height = "95vh";
export default function App() {
  const [nodeIdCounter, setNodeIdCounter] = React.useState(0);
  const [nodeList, setNodeList] = React.useState([]);
  const [linkList, setLinkList] = React.useState([]);

  const [selectedNode, setSelectedNode] = React.useState(null);
  const [selectedEdge, setSelectedEdge] = React.useState(null);

  const [openModal, setOpenModal] = React.useState(false);
  const [treeDepth, setTreeDepth] = React.useState(4);
  const [numChildren, setNumChildren] = React.useState(3);

  const canvasRef = React.useRef(null);

  const clickCreateNode = (e) => {
    setSelectedEdge(null);
    setSelectedNode(null);
    const rect = e.target.getBoundingClientRect();
    createNode(e.clientX - rect.left - 50, e.clientY - rect.top - 50);
  }
  const createNode = (x, y) => {
    setNodeList((prevVal) => [...prevVal, { id: nodeIdCounter, x: x, y: y }]);
    setNodeIdCounter((prevVal) => prevVal + 1);
  }

  const drawEdge = (source, target) => {
    removeEdge(nodeList[source], nodeList[target]);
    removeEdge(nodeList[target], nodeList[source]);
    setLinkList((prevVal) => [...prevVal, { source: nodeList[source], target: nodeList[target] }]);
  }

  const removeEdge = (source, target) => {
    for (let edgeIdx in linkList) {
      if (areEdgesEqual(linkList[edgeIdx], { source, target })) {
        const newLinkList = [...linkList];
        newLinkList.splice(parseInt(edgeIdx), 1);
        setLinkList(() => newLinkList);
        return true;
      }
    }
    return false;
  }

  const removeNode = (idx) => {
    const newNodeList = [...nodeList]
    newNodeList.splice(idx, 1);
    const newLinkList = [];
    linkList
      .filter(
        edge => !areNodesEqual(edge.source, nodeList[idx]) && !areNodesEqual(edge.target, nodeList[idx])
      ).forEach(edge => newLinkList.push(edge));
    setNodeList(() => newNodeList);
    setLinkList(() => newLinkList);
  }

  const clickNode = (node) => {
    setSelectedEdge(null);
    if (selectedNode !== null && areNodesEqual(node, nodeList[selectedNode])) {
      setSelectedNode(null);
      return;
    }

    for (const idx in nodeList) {
      if (areNodesEqual(node, nodeList[idx])) {
        if (selectedNode !== null) {
          drawEdge(selectedNode, idx);
          setSelectedNode(null);
          return;
        }
        
        setSelectedNode(() => idx);
        return;
      }
    }
  };

  const clickEdge = (edge) => {
    setSelectedNode(null);
    const idx = linkList.findIndex(
      e => areNodesEqual(edge.source, e.source) && areNodesEqual(edge.target, e.target)
    );
    setSelectedEdge(() => idx === -1 ? null : idx);
  }

  const keyPress = (e) => {
    if (e.key === "Backspace" && selectedNode !== null) {
      removeNode(selectedNode);
      setSelectedNode(null);
    } else if (e.key === "Backspace" && selectedEdge !== null) {
      if (!removeEdge(linkList[selectedEdge].source, linkList[selectedEdge].target)) {
        removeEdge(linkList[selectedEdge].target, linkList[selectedEdge].source);
      }
      setSelectedEdge(null);
    } else if (e.key === "Enter" && openModal) {
      makeTree();
    } else if (e.key === 'b') {
      const newNodeList = [...nodeList];
      let counter = 1;
      for (const nodeId of bfs(newNodeList, linkList, newNodeList[0], newNodeList[newNodeList.length - 1])) {
        newNodeList.find(node => node.id === nodeId).value = counter;
        counter++;
      }
      setNodeList(() => newNodeList);
    }
  }

  const makeTree = () => {
    setNodeList(() => []);
    setLinkList(() => []);
    const nodeCoords = [];
    const nodeLinks = [];

    if (!canvasRef.current) return;

    const rect = canvasRef.current.getBoundingClientRect();
    const midpointX = Math.round(rect.left + rect.width / 2);

    nodeCoords.push({ x: midpointX - 70, y: rect.top - 20 });

    let rowSize = numChildren;
    let n = treeDepth;

    let y = rect.top + 100;
    let prevCoords = [nodeCoords[0]];
    for (let i = 0 ; i < n ; i++) {
      let spacing = ((rect.width) / (rowSize + 1));
      let prevIdx = 0;
      let counter = 0;
      const currCoords = [];
      for (let j = rowSize - 1 ; j >=0 ; j--) {
        const newNode = { x: rect.left + spacing * (j + 1) - 70, y: y };
        nodeCoords.push(newNode);
        currCoords.push(newNode);
        nodeLinks.push({ source: prevCoords[prevIdx], target: newNode });
        counter++;
        if (counter % numChildren === 0) {
          counter = 0;
          prevIdx++;
        }
      }

      prevCoords = currCoords;

      rowSize *= numChildren;
      y += 120;
    }

    setNodeList(() => nodeCoords);
    setLinkList(() => nodeLinks);
    setOpenModal(false);
  };

  React.useEffect(() => {
    document.addEventListener('keyup', keyPress);
    return () => {
      document.removeEventListener('keyup', keyPress);
    }
  })

  return (
    <div
      style={{
        height: '100vh',
        width: '100vw',
        margin: '20px',
        overflow: 'hidden',
      }}
    >
      <svg width={width} height={height}>
        <rect ref={canvasRef} width={width} height={height} rx={14} fill={"#272b4d"}
          onClick={clickCreateNode}
        />
        <Graph
          graph={{ nodes: nodeList, links: linkList }}
          top={50}
          left={50}
          nodeComponent={(node) =>
            <CustomNode
              node={node}
              onClick={() => clickNode(node.node)}
              isSelected={selectedNode !== null && areNodesEqual(node.node, nodeList[selectedNode])}
            />
          }
          linkComponent={({ link }) => (
            <line
              x1={link.source.x}
              y1={link.source.y}
              x2={link.target.x}
              y2={link.target.y}
              strokeWidth={6}
              stroke={
                selectedEdge !== null && areEdgesEqual(linkList[selectedEdge], link) ?
                  "pink" : (link.colour ? link.colour : "white")
              }
              strokeOpacity={0.6}
              onClick={() => clickEdge(link)}
            />
          )}
        />
      </svg>
      <Park
        style={{ height: '50px', width: '50px', cursor: 'pointer'}}
        onClick={() => setOpenModal(true)}
      />
      <Modal
        open={openModal}
        onClose={() => setOpenModal(false)}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box
          sx={{
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
            width: 400,
            bgcolor: 'background.paper',
            border: '2px solid #000',
            boxShadow: 24,
            p: 4,
          }}
        >
          <label>depth</label>
          <input type="number" autoFocus
            onChange={(e) => setTreeDepth(Number(e.currentTarget.value))} value={treeDepth}
          />
          <br />
          <label>numChildren</label>
          <input type="number"
            onChange={(e) => setNumChildren(Number(e.currentTarget.value))} value={numChildren}
          />
        </Box>
      </Modal>
      
    </div>
  );
}
